﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.Custom.Interface;
using MyDebug;

namespace TestCstRatingConsole
{

    public class BrickItemInfo
    {
        /// <summary>
        /// 位置的存储精确度，到几位小数
        /// <para>对于存储时，×其倒数</para>
        /// <para>对读取时，直接×该精度</para>
        /// </summary>
        const float C_PosAccuracy = 0.01f;

        public bool hasData;

        /// <summary>
        /// 隶属于哪个类别的砖块:用于实例化时取出对应物体
        /// </summary>
        public int pref_GroupID;
        /// <summary>
        /// 对应类别下的第几个砖块。ID：用于实例化时取出对应物体
        /// </summary>
        public int pref_BrickID;

        //--目前不一定需要的--
        public IntVector2 areaId, blockId;
        public IntVector3 unitId;
        //--end目前不一定需要的--

        /// <summary>
        /// 诞生时点的Position，不代表目前实际位置：
        /// </summary>
        public Vector3 position;

        /// <summary>
        /// 诞生时点的Rotation
        /// </summary>
        public Vector3 rotationEuler;

        /// <summary>
        /// obj实例：
        /// </summary>
        public object objInstance;

        //TODO: 后期扩展：
        public CustomData[] customDatas;

        /// <summary>
        /// 转换为可保存的字符串
        /// </summary>
        public string ToSavedString()
        {
            //--换用新的连接字符串的方式
            //:3,4,  0,90,0,  3,         0
            //Pos     Rot   group  id
            //return ":"+
            //    position.x + ',' + position.y + ','                                                              //Position
            //    //封印：+position.z
            //    +(int) rotationEuler.x + ',' +(int) rotationEuler.y + ',' +(int) rotationEuler.z + ','             //Rotation
            //    + pref_GroupID + ','                                                                                //GroupID
            //    + pref_BrickID;                                                                                         //prefabID
            int acc = (int)(1 / C_PosAccuracy); //存储的精度
            //brick基本信息
            string baseInfos = string.Format(":{0},{1},{2},{3},{4},{5},{6},{7}",
                                        (int)(position.x * acc), (int)(position.y * acc), (int)(position.z * acc), //0~2：为xyz，精确到2位小数，此处直接x100，存整
                                        (int)rotationEuler.x, (int)rotationEuler.y, (int)rotationEuler.z,            //3，4，5：旋转，精简到整数
                                        pref_GroupID,                                                                                   //6：组的id，用于恢复创建物体
                                        pref_BrickID);                                                                                    //7：物体在组中的id，用于恢复创建物体
            //无自定义数据则返回
            if (this.customDatas == null)
            {
                return baseInfos;
            }
            else
            {
                //后面还需要跟上自定义信息：
                //--创建stringBuilder：
                System.Text.StringBuilder infosWithCustomdatas = new System.Text.StringBuilder(baseInfos);
                //--前面跟逗号','而后面不跟,
                for (int i = 0; i < this.customDatas.Length; i++)
                {
                    infosWithCustomdatas.Append(",");
                    infosWithCustomdatas.Append(this.customDatas[i].dataValue);
                }
                return infosWithCustomdatas.ToString();
            }
        }

        /// <summary>
        /// 从字符串中提取数据，如果成功则返回true
        /// </summary>
        public bool FromSavedString(string src)
        {
            string[] udata;                                 //单元中所能读取到的数据
            const int C_BASE_INFO_COUNT = 8;                //基础信息长度
            //尝试拆分出数据
            udata = src.Split(',');
            if (udata.Length < C_BASE_INFO_COUNT)
            {
                //该单元数据有误
                Debug.LogError("unit error: [len: " + udata.Length + "] [data:" + src + "]");
                return false;
            }

            #region 解析基础数据部分：
            //--分出基础数据：
            string posX = udata[0],
                    posY = udata[1],
                    posZ = udata[2];
            string rotX = udata[3],
                    rotY = udata[4],
                    rotZ = udata[5];
            string group = udata[6],
                    brickID = udata[7];


            int intVal = 0;

            //Position parse
            //--x
            int.TryParse(posX, out intVal);
            position.x = intVal * C_PosAccuracy;               //精确到N位小数
            //--y
            int.TryParse(posY, out intVal);
            position.y = intVal * C_PosAccuracy;
            //--z
            int.TryParse(posZ, out intVal);
            position.z = intVal * C_PosAccuracy;

            //Rotation parse
            //--x
            int.TryParse(rotX, out intVal);
            rotationEuler.x = intVal;
            //--y
            int.TryParse(rotY, out intVal);
            rotationEuler.y = intVal;
            //--z
            int.TryParse(rotZ, out intVal);
            rotationEuler.z = intVal;

            //group id:
            int.TryParse(group, out intVal);
            pref_GroupID = intVal;

            //brickID
            int.TryParse(brickID, out intVal);
            pref_BrickID = intVal;
            #endregion

            #region 解析自定义定制数据部分：
            if (udata.Length > C_BASE_INFO_COUNT)
            {
                int udataStartPos = C_BASE_INFO_COUNT;
                int udataEndPos = udata.Length;
                //--计算自定义数据的长度
                int customDatasCount = udataEndPos - udataStartPos;
                float floatVal;
                //--自定义数据数组创建
                CustomData[] customDatasRead = new CustomData[customDatasCount];

                for (int currUdataPos = udataStartPos, currCustomdataPos = 0; currUdataPos < udataEndPos; currUdataPos++, currCustomdataPos++)
                {
                    customDatasRead[currCustomdataPos] = new CustomData();

                    //--解析udata后面的数据
                    float.TryParse(udata[currUdataPos], out floatVal);

                    //--解析后的数据放到对应customDatas的位置
                    customDatasRead[currCustomdataPos].dataValue = floatVal;
                }

                //--完成自定义数据解析
                this.customDatas = customDatasRead;
            }
            else
            {
                //--无数据，则设置Null
                this.customDatas = null;
            }
            #endregion

            return true;
        }

    }
}
