﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

    ﻿using System.Collections;


namespace Game.Custom.Interface
{
    /// <summary>
    /// 可被用户自定义数据的接口
    /// </summary>
    interface ICustomizable
    {
        /// <summary>
        /// 获得这个物体名字，用于提供给用户辨别
        /// </summary>
        string GetCustomizeObjName();

        ///// <summary>
        /////TODO:后期有多个组件同时挂载在同一个GameObj时需要在这里改进HashCode以区分
        ///// 获得使用接口类的HashCode（为了加速请自行计算对应类的hashCode并硬编码）
        ///// </summary>
        //int GetClassHashCode();

        /// <summary>
        /// 是否改变了定制数据
        /// </summary>
        bool IsCustomizeDataChanged();

        /// <summary>
        /// 获取可被定制化的数据，均将被转为float
        /// </summary>
        /// <remarks>序列由继承者定义，且与set保持一致</remarks>
        CustomData[] GetCustomDatas();

        /// <summary>
        /// 设置对应序列的值
        /// </summary>
        /// <remarks>序列顺序由继承者定义</remarks>
        void SetCustomDatas(CustomData[] floatDatas);
    }

    public struct CustomData
    {

        /// <summary>
        /// 初始化自定义数据
        /// </summary>
        /// <param name="varName">这个数据的变量名字</param>
        /// <param name="varValue">变量值</param>
        /// <param name="varType">类型</param>
        /// <param name="varMinValue">最小值，0以上</param>
        /// <param name="varMaxValue">最大值，负值则无穷大</param>
        public CustomData(string varName, float varValue, CustomDataType varType, float varMinValue, float varMaxValue)
        {
            this.dataName = varName;
            this.m_dataValue = varValue;
            this.dataType = varType;
            this.maxValue = varMaxValue;
            this.minValue = varMinValue;
        }

        /// <summary>
        /// 数值名
        /// </summary>
        public string dataName;

        float m_dataValue;

        /// <summary>
        /// 数值
        /// </summary>
        public float dataValue
        {
            get { return m_dataValue; }

            set
            {
                if (value > maxValue && maxValue > 0)
                {
                    m_dataValue = maxValue;
                }
                else if (value < minValue && minValue >= 0)
                {
                    m_dataValue = minValue;
                }
                else
                {
                    m_dataValue = value;
                }
            }
        }

        public CustomDataType dataType;

        /// <summary>
        /// 数值最大允许范围
        /// </summary>
        public float maxValue;

        /// <summary>
        /// 数值最小允许范围
        /// </summary>
        public float minValue;
    }

    /// <summary>
    /// 可自定义输入的数据类型
    /// </summary>
    public enum CustomDataType
    {
        /// <summary>
        /// 有滑条的范围值
        /// </summary>
        RangeValue,

        /// <summary>
        /// 直接输入框的值
        /// </summary>
        InputValue,

        /// <summary>
        /// 不能修改的Label值
        /// </summary>
        LabelValue,
    }
}
