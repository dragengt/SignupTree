﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestCstRatingConsole
{
    /// <summary>
    ///  整数类型的Vector3向量, 无其他只get属性，可用于序列化保存
    /// </summary>
    public struct IntVector3
    {
        public int x, y , z;
        public IntVector3(int ix, int iy)
        {
            x = ix;
            y = iy;
            z = 0;
        }

        public IntVector3(int ix, int iy,int iz)
        {
            x = ix;
            y = iy;
            z = iz;
        }

        //运算符重载
        public static bool operator ==(IntVector3 gl, IntVector3 gr)
        {
            return gl.x == gr.x 
                && gl.y == gr.y
                && gl.z == gr.z;
        }
        public static bool operator !=(IntVector3 gl, IntVector3 gr)
        {
            return gl.x != gr.x 
                    || gl.y != gr.y
                    || gl.z != gr.z;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
#if UNITY_EDITOR
        public override string ToString()
        {
            return "(" + x + " , " + y+")";
        }
        public string ToString(string format)
        {
            return ToString();
        }
#endif
    }


    /// 整数类型的Vector3向量
    public struct IntVector2
    {
        public int x, z;
        public IntVector2(int ix, int iz)
        {
            x = ix;
            z = iz;
        }


        //+
        public static IntVector2 operator +(IntVector2 gl, IntVector2 gr)
        {
            gl.x += gr.x;
            gl.z += gr.z;
            return gl;
        }

        //-
        public static IntVector2 operator -(IntVector2 gl, IntVector2 gr)
        {
            gl.x -= gr.x;
            gl.z -= gr.z;
            return gl;
        }

        /// <summary>
        /// 返回两者的x、y差值（绝对值）
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public IntVector2 DeltaAbsTo( IntVector2 b)
        {
            b.x = Mathf.Abs(this.x - b.x);
            b.z = Mathf.Abs(this.z - b.z);
            return b;
        }

        /// <summary>
        /// 计算两者的x、y差值，并返回差值之和
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int LinerDistanceTo( IntVector2 b)
        {
            b.x = Mathf.Abs(this.x - b.x);
            b.z = Mathf.Abs(this.z - b.z);
            return b.x + b.z;
        }

        //运算符重载
        public static bool operator ==(IntVector2 gl, IntVector2 gr)
        {
            return gl.x == gr.x && gl.z == gr.z;
        }
        public static bool operator !=(IntVector2 gl, IntVector2 gr)
        {
            return gl.x != gr.x ||
                gl.z != gr.z;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
#if UNITY_EDITOR
        public override string ToString()
        {
            return "(" + x + " , " + y+")";
        }
        public string ToString(string format)
        {
            return ToString();
        }
#endif
    }

}
