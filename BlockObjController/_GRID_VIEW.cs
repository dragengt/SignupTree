﻿

namespace TestCstRatingConsole.BlockObjController
{

    /// <summary>
    /// 九宫格视图信息记录的最小单位：区+块
    /// </summary>
    struct _GRID_VIEW
    {
        public IntVector2 areaID, blockID;
        public void SetNull()
        {
            areaID.x = -999;
            areaID.z = -1;
            blockID.x = -1;
            blockID.z = -1;
        }                   //初始
        public bool IsNull()
        {
            return areaID.x == -999;
        }
        public bool IsEquals(IntVector2 area, IntVector2 block)
        {
            return this.areaID == area
                && this.blockID == block;
        }
    }          //定义Grid数据:含区、块信息
}
