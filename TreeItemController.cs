﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCstRatingConsole
{
    /// <summary>
    /// Controller to manage tree items
    /// </summary>
    internal class TreeItemController
    {
        /// <summary>
        /// Grid Size, to controll distance of each tree
        /// <remarks>Assume it is in cube shape</remarks>
        /// </summary>
        public static  int C_GridSize = 1;

        //SafeDictionary
        static bool[][] g_treeMaps;

        static int g_sizeX, g_sizeZ;

        static void InitMap(int sizeX, int sizeZ)
        {
            g_treeMaps = new bool[sizeX][];
            
            g_sizeX = sizeX;
            g_sizeZ = sizeZ;
        }

#if !UNITY_EDITOR
        public struct Vector3
        {
            public float x,y,z;
        }
#endif

        public static Vector3 FormatPositionXZ(Vector3 srcPos)
        {
            //Let it align to grid:
            srcPos.x = (int)(srcPos.x / C_GridSize) * C_GridSize;
            srcPos.z = (int)(srcPos.z / C_GridSize) * C_GridSize;

            //For it is struct, we can return the parameter:
            return srcPos;
        }

        public static bool IsTreeMapped(Vector3 pos)
        {
            var formatedPos = FormatPositionXZ(pos);
            int gridPosX =(int) formatedPos.x,
                gridPosZ =(int) formatedPos.z;

            return IsTreeMapped(gridPosX, gridPosZ);
        }

        private static bool IsTreeMapped(int x,int z)
        {
            if (g_treeMaps[x] == null)
            {
                g_treeMaps[x] = new bool[g_sizeZ];
            }

            return g_treeMaps[x][z];
        }

        private string Serialize()
        {
            return "NO FINISHED YET";
        }

        private void Deserialize(string str)
        {
            //TODO: make this class singleton:
        }


    }
}
